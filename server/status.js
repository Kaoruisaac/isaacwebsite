
function status(code){
  let str_obj;
  switch(code){
    case status.SUCCESS:
      str_obj = {
        code:status.SUCCESS,
        msg:'api called successful!',
        data:null,
      }
    break;
    case status.FAILD:
      str_obj = {
        code:status.FAILD,
        msg:'api went wrong!',
        data:null,
      }
    break;
    case status.ALREADY_BUY:
      str_obj = {
        code:status.ALREADY_BUY,
        msg:'someone has already bought it !',
        data:null,
      }
    break;
  }
  return str_obj;
}

status.SUCCESS = 1000;
status.FAILD = 1002;
status.ALREADY_BUY = 1003;

module.exports = status;